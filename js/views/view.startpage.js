"use strict";

import Core_View from "../core/core.spa-view.js?v=0.1";

export default class StartpageView extends Core_View {
  constructor(slug, template) {
    super(slug, template);
  }

  init() {
    super.init();
  }
}
