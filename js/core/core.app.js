"use strict";

import Core_SPA_Router from "./core.spa-router.js";

/**********************************************************************
 *     Class-Bundle for Web-Apps.
 *     App-Shell needs an ID "#core_app".
 *
 *     @param:
 *     webRoot - Give me the root-URL of your App
 *     templatesPath - Give me the Path to your templates
 *       relative to your webRoot.
 *     routes - Give me an Object with "slug" : "template" Routes
 *     config - Want to store some Config in your App? Here you go!
 *     ...languages - Give me all languages you want your App to support.
 *
 *     Neuwersch - 2020-03-25
 **********************************************************************/

export default class Core_App {
  constructor(webRoot, templatesPath, routes, ...languages) {
    window.Core = this;
    this.system = {
      webRoot: webRoot, // root-url of the app
      templatesPath: templatesPath, // path to folder containing templates
      debugmode: true // if activated, show debug logs
    };
    this.router = new Core_SPA_Router(routes);
  }
}
