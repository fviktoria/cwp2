"use strict";
/*******************************************************
 *     Hash-based Routes for Single Page Applications.
 *     Routes can are treated like Views. Each Route is
 *     therefore bound to one single (unique) View.
 *
 *     Neuwersch - 2020-03-25
 *******************************************************/

const app = document.getElementById("core_app");

export default class Core_View {
  constructor(slug, template) {
    this.slug = slug;
    this.template = template;
    window.addEventListener("templateChanged", this.listen.bind(this));
  }

  listen(e) {
    if (e.details.slug == this.slug) {
      // this.init();
    }
  }

  init() {
    if (window.Core.system.debugmode) {
      console.log("View loaded: " + this.slug);
    }
  }

  isActive() {
    return window.location.hash.substr(1).replace("#", "") === this.slug;
  }

  renderMarkup() {
    app.innerHTML = this.slug;
  }
}
