"use strict";

import Core_App from "./core/core.app.js";
import LoginView from "./views/view.login.js";
import StartpageView from "./views/view.startpage.js";

let routes = [
  new StartpageView("/", "startpage"),
  new LoginView("/login", "login")
];

const C_Holidays_App = new Core_App(
  "localhost:8888/cwp2/c-holidays/",
  "templates",
  routes,
  "de",
  "en"
);
